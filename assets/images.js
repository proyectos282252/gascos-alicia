import sliderImage from "../public/assets/image-slider.png";
import cylinder5kg from "../public/assets/cylinder5kg.png";
import cylinder11kg from "../public/assets/cylinder11kg.png";
import cylinder15kg from "../public/assets/cylinder15kg.png";
import cylinder45kg from "../public/assets/cylinder45kg.png";
import flameLeft from "../public/assets/flame-left-format-cylinder.png";
import flameRight from "../public/assets/flame-right-format-cylinder.png";
import quillon from "../public/assets/quillon.png";
import naldea from "../public/assets/naldea.png";
import logo from "../public/assets/logo.png";
import aboutUsLeft from "../public/assets/aboutUsLeft.png";
import aboutUsRight from "../public/assets/aboutUsRight.png";
import quoteHere from "../public/assets/quoteHere.png";


export {
    sliderImage,
    cylinder5kg,
    cylinder11kg,
    cylinder15kg,
    cylinder45kg,
    flameLeft,
    flameRight,
    quillon,
    naldea,
    logo,
    aboutUsLeft,
    aboutUsRight,
    quoteHere,
  };
"use client";
import { useState } from "react";
import styles from "./page.module.css";
import { Roboto_Condensed } from "next/font/google";
import Image from "next/image";
import * as images from "../../assets/images.js";

const roboto_condensed = Roboto_Condensed({
  subsets: ["latin"],
  weight: ["100", "300", "400", "500", "700", "900"],
});

export default function Home() {
  const [countCylinder, setCountCylinder] = useState(`Cantidad`);
  const [totalPrice, setTotalPrice] = useState(0);
  const [selectedCylinder, setSelectedCylinder] = useState("1");
  const [activeItem, setActiveItem] = useState(0);

  const handleSubmit = async () => {
    const formData = {
      mail: document.getElementById("txtMail").value,
      phone: document.getElementById("txtPhone").value,
      formatoCilindro: document.getElementById("formatoCilindro").value,
      cantidad: document.getElementById("cantidad").value,
      valorTotal: document.getElementById("valorTotal").value,
      comment: document.getElementById("txtComment").value,
    };

    try {
      const response = await fetch("/api/formHandler", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(formData),
      });
      const data = await response.json();
      console.log(data);
    } catch (error) {
      console.error(error);
    }
  };

  const handleSelectedCylinder = (e) => {
    const count = e.target.value;
    setSelectedCylinder(count);
    console.log(selectedCylinder);

    if (count !== "1") {
      const inputElement = document.getElementById("cantidad");
      inputElement.removeAttribute("disabled");
    } else if (count === "1") {
      const inputElement = document.getElementById("cantidad");
      inputElement.setAttribute("disabled", "true");
      setTotalPrice(0);
      setCountCylinder(0);
    }
  };

  const handlePrecioChange = (e) => {
    const count = e.target.value;
    setCountCylinder(count);

    setCountCylinder((prevCount) => {
      if (selectedCylinder === "2") {
        let price = prevCount * 11000;
        setTotalPrice(price);
      } else if (selectedCylinder === "3") {
        let price = prevCount * 22000;
        setTotalPrice(price);
      } else if (selectedCylinder === "4") {
        let price = prevCount * 22500;
        setTotalPrice(price);
      } else if (selectedCylinder === "5") {
        let price = prevCount * 85000;
        setTotalPrice(price);
      } else {
        console.log("Valor seleccionado inválido");
      }

      return prevCount;
    });
  };

  const handleItemClick = (index) => {
    setActiveItem(index);
  };

  return (
    <>
      <div className={styles.primaryNav} id="inicio">
        <div className="w-100">
          <div className="container">
            <div className="row">
              <div className="col-sm-12 col-md-6">
                <p className="fw-bold text-white d-flex">
                  <span className="material-symbols-outlined">call</span>
                  Teléfono de contacto: +56 9 9144 6725
                </p>
              </div>
              <div className="col-6 justify-content-end d-flex d-none d-md-flex">
                <p className="fw-bold text-white d-flex">
                  <span className="material-symbols-outlined">schedule</span>
                  Horario: Lunes a Domingo de 10:00 a 22:00 HRS.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <section className={`${styles.sectionSlider}`}>
        <nav className="navbar navbar-expand-lg pt-2">
          <div className="container-fluid">
            <Image
              src={images.logo}
              width={129}
              height={68}
              alt="Picture of the author"
              className={`d-lg-none ${styles.logo}`}
            />
            <button
              className="navbar-toggler"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#navbarSupportedContent"
              aria-controls="navbarSupportedContent"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className="navbar-toggler-icon"></span>
            </button>
            <div
              className="collapse navbar-collapse"
              id="navbarSupportedContent"
            >
              <header className="w-100">
                <div className="container pt-4">
                  <div className="row">
                    <div className="col-4 d-none d-md-block">
                      <Image
                        src={images.logo}
                        width={129}
                        height={68}
                        alt="Picture of the author"
                        className={`d-none d-md-block ${styles.logo}`}
                      />
                    </div>
                    <div className="col-sm-12 col-md-8">
                      <ul
                        className={`row ${styles.listItemNav}`}
                      >
                        <li>
                          <a
                            href="#inicio"
                            className={
                              activeItem === 0 ? `${styles.itemActive}` : ""
                            }
                            onClick={() => handleItemClick(0)}
                          >
                            Inicio
                          </a>
                        </li>
                        <li>
                          <a
                            href="#formatos"
                            className={
                              activeItem === 1 ? `${styles.itemActive}` : ""
                            }
                            onClick={() => handleItemClick(1)}
                          >
                            Formatos de Cilindro
                          </a>
                        </li>
                        <li>
                          <a
                            href="#encuentranos"
                            className={
                              activeItem === 2 ? `${styles.itemActive}` : ""
                            }
                            onClick={() => handleItemClick(2)}
                          >
                            Encuéntranos en
                          </a>
                        </li>
                        <li>
                          <a
                            href="#nosotros"
                            className={
                              activeItem === 3 ? `${styles.itemActive}` : ""
                            }
                            onClick={() => handleItemClick(3)}
                          >
                            Sobre Nosotros
                          </a>
                        </li>
                        <li>
                          <a
                            href="#cotiza"
                            className={
                              activeItem === 4 ? `${styles.itemActive}` : ""
                            }
                            onClick={() => handleItemClick(4)}
                          >
                            Cotiza Aquí
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </header>
            </div>
          </div>
        </nav>
        <article className={`${styles.contentSlider}`}>
          <div className="container">
            <div className="row">
              <div className="col-12 col-lg-6">
                <h1 className={`${roboto_condensed.className}`}>
                  Gasco´S Alicia
                </h1>
                <p className="col-12 col-lg-10">
                  Venta de gas a precios exequibles para la comuna de Quillón y
                  el Sector de Nueva Aldea.
                  <br />
                  (Venta de gas directo en local)
                </p>
                <a
                  href="https://wa.me/+56991446725?text=Hola, buen día, me interesaría saber más acerca de los productos que ofrece."
                  target="_blank"
                >
                  <button>
                    <span className="material-symbols-outlined">chat</span>
                    Contáctanos
                  </button>
                </a>
              </div>
              <div className="d-none d-lg-block col-6">
                <Image
                  src={images.sliderImage}
                  alt="Picture of the author"
                  className={`${styles.sliderImage}`}
                  quality={100}
                />
              </div>
            </div>
          </div>
        </article>
      </section>
      <article className={`${styles.sliderFooter}`}>
        <div className="container">
          <div className="row">
            <div className="col-md-12 col-lg-4 pt-4">
              <div className="row">
                <div className="col-2">
                  <div className={`${styles.icoSliderFooter}`}>
                    <span className="material-symbols-outlined">
                      propane_tank
                    </span>
                  </div>
                </div>
                <div className={`col-10 ${styles.textSliderFooter}`}>
                  <h3 className={`ms-3 ${roboto_condensed.className}`}>
                    4 FORMATOS DE CILINDROS
                  </h3>
                  <p className="ms-3">Cilindros de 5, 11, 15 y 45 KG.</p>
                </div>
              </div>
            </div>
            <div className="col-md-12 col-lg-4 pt-4">
              <div className="row">
                <div className="col-2">
                  <div className={`${styles.icoSliderFooter}`}>
                    <span className="material-symbols-outlined">
                      local_shipping
                    </span>
                  </div>
                </div>
                <div className={`col-10 ${styles.textSliderFooter}`}>
                  <h3 className={`ms-3 ${roboto_condensed.className}`}>
                    ATENCIÓN INMEDIATA
                  </h3>
                  <p className="ms-3">Estamos a tu disposición</p>
                </div>
              </div>
            </div>
            <div className="col-md-12 col-lg-4 pt-4">
              <div className="row">
                <div className="col-2">
                  <div className={`${styles.icoSliderFooter}`}>
                    <span className="material-symbols-outlined">local_atm</span>
                  </div>
                </div>
                <div className={`col-10 ${styles.textSliderFooter}`}>
                  <h3 className={`ms-3 ${roboto_condensed.className}`}>
                    GAS AL MEJOR PRECIO
                  </h3>
                  <p className="ms-3">Desde $11.000</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </article>
      <main className={`${styles.sectionMain}`}>
        <div className="container">
          <section className={`${styles.cylinderFormats}`} id="formatos">
            <div className={`row ${styles.rowSubTitle}`}>
              <div className="col-8">
                <h2 className={`${roboto_condensed.className}`}>
                  Nuestros Formatos de Cilindros
                </h2>
              </div>
              <div className="col-4 d-flex align-content-center flex-wrap">
                <hr />
              </div>
            </div>
            <div className="row position-relative">
              <Image
                src={images.flameLeft}
                alt="Picture of the author"
                className={styles.flameLeft}
                quality={100}
              />
              <div className="col-lg-3 col-md-6 col-sm-12 z-3">
                <article className={`text-center ${styles.articleCylinder}`}>
                  <Image
                    src={images.cylinder5kg}
                    alt="Picture of the author"
                    className={styles.imgCylinder}
                    quality={100}
                  />
                  <div className={`${styles.contentCylinder}`}>
                    <p className={`${styles.descriptionCylinder}`}>
                      Gas de 5 KG.
                    </p>
                    <p
                      className={`${styles.priceCylinder} ${roboto_condensed.className}`}
                    >
                      $11.000
                    </p>
                    <a
                      href="https://wa.me/+56991446725?text=Hola, buen día, me gustaria comprar un gas de 5 Kg."
                      target="_blank"
                    >
                      <button className={`${styles.btnGeneral}`}>
                        Solicitar
                      </button>
                    </a>
                  </div>
                </article>
              </div>
              <div className="col-lg-3 col-md-6 col-sm-12">
                <article className={`text-center ${styles.articleCylinder}`}>
                  <Image
                    src={images.cylinder11kg}
                    alt="Picture of the author"
                    className={styles.imgCylinder}
                    quality={100}
                  />
                  <div className={`${styles.contentCylinder}`}>
                    <p className={`${styles.descriptionCylinder}`}>
                      Gas de 11 KG.
                    </p>
                    <p
                      className={`${styles.priceCylinder} ${roboto_condensed.className}`}
                    >
                      $22.000
                    </p>
                    <a
                      href="https://wa.me/+56991446725?text=Hola, buen día, me gustaria comprar un gas de 11 Kg."
                      target="_blank"
                    >
                      <button className={`${styles.btnGeneral}`}>
                        Solicitar
                      </button>
                    </a>
                  </div>
                </article>
              </div>
              <div className="col-lg-3 col-md-6 col-sm-12">
                <article className={`text-center ${styles.articleCylinder}`}>
                  <Image
                    src={images.cylinder15kg}
                    alt="Picture of the author"
                    className={styles.imgCylinder}
                    quality={100}
                  />
                  <div className={`${styles.contentCylinder}`}>
                    <p className={`${styles.descriptionCylinder}`}>
                      Gas de 15 KG.
                    </p>
                    <p
                      className={`${styles.priceCylinder} ${roboto_condensed.className}`}
                    >
                      $22.500
                    </p>
                    <a
                      href="https://wa.me/+56991446725?text=Hola, buen día, me gustaria comprar un gas de 15 Kg."
                      target="_blank"
                    >
                      <button className={`${styles.btnGeneral}`}>
                        Solicitar
                      </button>
                    </a>
                  </div>
                </article>
              </div>
              <div className="col-lg-3 col-md-6 col-sm-12 z-3">
                <article className={`text-center ${styles.articleCylinder}`}>
                  <Image
                    src={images.cylinder45kg}
                    alt="Picture of the author"
                    className={styles.imgCylinder}
                    quality={100}
                  />
                  <div className={`${styles.contentCylinder}`}>
                    <p className={`${styles.descriptionCylinder}`}>
                      Gas de 45 KG.
                    </p>
                    <p
                      className={`${styles.priceCylinder} ${roboto_condensed.className}`}
                    >
                      $85.000
                    </p>
                    <a
                      href="https://wa.me/+56991446725?text=Hola, buen día, me gustaria comprar un gas de 45 Kg."
                      target="_blank"
                    >
                      <button className={`${styles.btnGeneral}`}>
                        Solicitar
                      </button>
                    </a>
                  </div>
                </article>
              </div>
              <Image
                src={images.flameRight}
                alt="Picture of the author"
                className={styles.flameRight}
                quality={100}
              />
            </div>
          </section>
          <section className={`${styles.sectionFindusIn}`} id="encuentranos">
            <div className="row">
              <div className="col-12 col-lg-6 position-relative">
                <div className={`row mb-0 ${styles.rowSubTitle}`}>
                  <div className="col-9">
                    <h2
                      className={`${roboto_condensed.className} ${styles.titleEncuentranos}`}
                    >
                      Encuéntranos en
                    </h2>
                  </div>
                  <div className="col-3 d-flex align-content-center flex-wrap">
                    <hr />
                  </div>
                </div>
                <p className="col-11 col-lg-12">
                  Dentro de la comuna de Quillón, camino hacia Nueva Aldea.
                  Exactamente en el Kilometro 7,8 / Sector Quitrico.
                </p>
                <Image
                  src={images.quillon}
                  className={`w-100 h-auto ${styles.imgQuillon}`}
                  alt="Picture of the author"
                  quality={100}
                />
                <Image
                  src={images.flameLeft}
                  alt="Picture of the author"
                  className={`w-auto ${styles.flameLeft}`}
                  quality={100}
                />
              </div>
              <div className="col-12 col-lg-6 position-relative">
                <Image
                  src={images.flameRight}
                  alt="Picture of the author"
                  className={`w-auto ${styles.flameRight}`}
                  quality={100}
                />
                <Image
                  src={images.naldea}
                  className={`w-100 h-auto ${styles.imgNaldea}`}
                  alt="Picture of the author"
                  quality={100}
                />
                <p className="col-11 col-lg-10">
                  Desde la perspectiva de la comuna de Ranquil, nos a ubicamos a
                  2.5 kilómetros, camino a Quillón. / Sector Quitrico.
                </p>
              </div>
            </div>
          </section>
          <section className={`${styles.sectionAboutUs}`} id="nosotros">
            <div className="row">
              <div className="col-4 d-flex align-content-center flex-wrap">
                <hr></hr>
              </div>
              <div className="col-4">
                <h2 className={`text-center ${roboto_condensed.className}`}>
                  Sobre Nosotros
                </h2>
              </div>
              <div className="col-4 d-flex align-content-center flex-wrap">
                <hr></hr>
              </div>
            </div>
            <p className="text-center d-block m-auto col-12 col-md-8 mt-3">
              Somos gente de esfuerzo al igual que tú, que busca emprender en
              esta área de venta, otorgando precios accequibles para todos y la
              mejor atención posible cara a nuestros clientes.
            </p>
            <div className={`row ${styles.imagesAboutUs}`}>
              <div className="col-12 col-lg-6 mb-5 position-relative">
                <Image
                  src={images.flameLeft}
                  alt="Picture of the author"
                  className={`w-auto ${styles.flameLeft}`}
                  quality={100}
                />
                <Image
                  src={images.aboutUsLeft}
                  className={`w-100 h-auto ${styles.aboutUsLeft}`}
                  alt="Picture of the author"
                  quality={100}
                />
              </div>
              <div className="col-12 col-lg-6 position-relative">
                <Image
                  src={images.aboutUsRight}
                  className={`w-100 h-auto ${styles.aboutUsRight}`}
                  alt="Picture of the author"
                  quality={100}
                />
                <Image
                  src={images.flameRight}
                  alt="Picture of the author"
                  className={`w-auto ${styles.flameRight}`}
                  quality={100}
                />
              </div>
            </div>
          </section>
          <section className={`${styles.sectionQuoteHere}`} id="cotiza">
            <div className="row">
              <div className="col-12 col-lg-6">
                <div className="row mb-4">
                  <div className="col-6">
                    <h2 className={`${roboto_condensed.className}`}>
                      Cotiza Aquí
                    </h2>
                  </div>
                  <div className="col-6 col-lg-4 d-flex align-content-center flex-wrap">
                    <hr></hr>
                  </div>
                </div>
                <form
                  action="mail.php"
                  method="post"
                  name="contacto"
                  id="contacto"
                  className="col-12 col-lg-10"
                >
                  <div className="c-form">
                    <input
                      name="txtMail"
                      id="txtMail"
                      type="email"
                      placeholder="Correo"
                      className="form-control form-control-lg"
                      required
                    />
                    <input
                      name="txtPhone"
                      id="txtPhone"
                      type="text"
                      placeholder="Teléfono"
                      className="form-control form-control-lg"
                      required
                    />
                    <select
                      name="formatoCilindro"
                      id="formatoCilindro"
                      className="form-select form-select-lg"
                      aria-label="Default select example"
                      onChange={handleSelectedCylinder}
                      value={selectedCylinder}
                      required
                    >
                      <option value="1">Formato de clindro</option>
                      <option value="2">5 Kg.</option>
                      <option value="3">11 Kg.</option>
                      <option value="4">15 Kg.</option>
                      <option value="5">45 Kg.</option>
                    </select>
                    <input
                      name="cantidad"
                      id="cantidad"
                      type="number"
                      placeholder="Cantidad"
                      className="form-control form-control-lg"
                      onInput={handlePrecioChange}
                      value={countCylinder}
                      min="1"
                      disabled
                      required
                    />
                    <input
                      name="valorTotal"
                      id="valorTotal"
                      type="text"
                      className="form-control form-control-lg"
                      value={`$${totalPrice}`}
                      disabled
                      readOnly
                    />
                  </div>
                  <textarea
                    id="txtComment"
                    name="txtComment"
                    placeholder="Comentarios adicionales..."
                    className="form-control form-control-lg"
                  ></textarea>
                  <div className="c-button">
                    <input
                      type="submit"
                      className={`${styles.btnEnviar}`}
                      value="Envianos tu cotización"
                      onClick={handleSubmit}
                    />
                  </div>
                </form>
              </div>
              <div className="d-none d-lg-block col-6 position-relative">
                <Image
                  src={images.flameLeft}
                  alt="Picture of the author"
                  className={`w-auto ${styles.flameLeft}`}
                  quality={100}
                />
                <Image
                  src={images.quoteHere}
                  className={`w-100 ${styles.quoteHere}`}
                  alt="Picture of the author"
                  quality={100}
                />
                <Image
                  src={images.flameRight}
                  alt="Picture of the author"
                  className={`w-auto ${styles.flameRight}`}
                  quality={100}
                />
              </div>
            </div>
          </section>
        </div>
        <footer className={`${styles.sectionFooter}`}>
          <div className={`w-100 ${styles.accentFooter}`}></div>
          <div className={`${styles.contentFooter}`}>
            <div className="container">
              <div className="row">
                <div className="col-12 col-md-5">
                  <h4>Nuestra Ubicación</h4>
                  <div className={`${styles.mapa}`}>
                    <iframe
                      src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d2230.1753397888283!2d-72.45534200401124!3d-36.67884446517745!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1ses-419!2scl!4v1704086000162!5m2!1ses-419!2scl"
                      allowFullScreen=""
                      loading="lazy"
                      referrerPolicy="no-referrer-when-downgrade"
                    ></iframe>
                  </div>
                </div>
                <div className="col-12 col-md-4">
                  <h4>Links</h4>
                  <ul>
                    <a href="#inicio">
                      <li>Inicio</li>
                    </a>
                    <a href="#formatos">
                      <li>Formatos de cilindro</li>
                    </a>
                    <a href="#encuentranos">
                      <li>Encuéntranos en</li>
                    </a>
                    <a href="#nosotros">
                      <li>Quienes Somos</li>
                    </a>
                    <a href="#cotiza">
                      <li>Cotiza Aquí</li>
                    </a>
                  </ul>
                </div>
                <div className="col-12 col-md-3">
                  <h4>Contacto</h4>
                  <ul className={`${styles.ulContacto}`}>
                    <li>Whatsapp: +56 9 9144 6725</li>
                    <li>Correo: info@gascosalicia.cl</li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </footer>
      </main>
    </>
  );
}
